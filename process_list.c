
#include<linux/init.h>
#include<linux/miscdevice.h>
#include<linux/module.h>
#include <linux/errno.h>
#include <linux/types.h>
#include<linux/sched.h>    // pid,ppid etc.
#include<linux/fs.h>
#include<linux/kernel.h>  //printk()
#include<asm/uaccess.h>  //copy_to_user()
#include<linux/slab.h>  // kmalloc()and kfree()
#include<linux/string.h>// String manipulation
MODULE_LICENSE("GPL");

static char* buffer;
static char* bu;
static int count=0;
static int n=0; 

static int my_open(struct inode *, struct file *);
static ssize_t my_read(struct file *, char*, size_t, loff_t *);
static int my_close(struct inode *, struct file *);
static  char *  process_state(struct task_struct *);
 
 


static int my_open(struct inode *in, struct file *fp)
{      
        struct task_struct *ptr;
        buffer = (char*) kmalloc(10000, GFP_KERNEL);
  
        if(buffer)
        {
        }
        else
         {   printk(KERN_ERR"\n Null Pointer "); 
             return 0;
         }
      
      bu = buffer;
             	   
    for_each_process(ptr)
    {    
        n = sprintf(buffer,"\n PID = %d     PPID = %d      CPU = %d    STATE = %s \n ", ptr->pid, ptr->parent->pid, task_cpu(ptr),process_state(ptr));  //ptr->state
                
       // printk(KERN_ALERT"\n %s",buffer);
       // printk(KERN_ALERT"\n buffer  value = %p",buffer);
          buffer = buffer + strlen(buffer);
       // printk(KERN_ALERT"\n incremented buffer value = %p",buffer);
          count = count + n;       
       // printk(KERN_ALERT"\n total number of bytes = %d",count);
     }  
 return 0;
 }
 
 
 static char*  process_state(struct task_struct *ptr)
 {
        char *a;
        long token;
	token  = ptr->state;
	if(token == 0)
        a = "TASK_RUNNING " ; 
	else if(token == 1)
	 a = "INTERRUPTIBLE" ;
	else if(token == 2)
	a = "TASK_UNINTERRUPTIBLE ";
        else if(token == 3)
        a = "TASK_INTERRUPTIBLE | TASK_UNINTERRUPTIBLE";
	else if(token == 4)
	a = "__TASK_STOPPED ";
	else if(token == 8)
	a = "__TASK_TRACED ";
	else if(token == 16)
	a = "EXIT_DEAD  ";
	else if(token == 32)
	a = "EXIT_ZOMBIE ";
	else if(token == 48)
	a = "EXIT_ZOMBIE | EXIT_DEAD";
       else if(token == 63)
        a = "TASK_RUNNING | TASK_INTERRUPTIBLE |  TASK_UNINTERRUPTIBLE | __TASK_STOPPED |__TASK_TRACED | EXIT_ZOMBIE | EXIT_DEAD";
	else if(token == 64)
	a = "TASK_DEAD  ";
	else if(token == 128)
	a = "TASK_WAKEKILL ";
	else if(token == 130)
	a = "TASK_WAKEKILL | TASK_UNINTERRUPTIBLE";
	else if(token == 132)
	a = "TASK_WAKEKILL | __TASK_STOPPED";
	else if(token == 136)
	a = "TASK_WAKEKILL | __TASK_TRACED";
	else if(token == 256)
	a = "TASK_WAKING  ";
	else if(token == 512)
	a = "TASK_PARKED   ";
	else if(token == 1024)
	a = "TASK_NOLOAD ";
	else if(token == 1026)
	a = "TASK_UNINTERRUPTIBLE | TASK_NOLOAD";
	else if(token ==  2048)
	a = "TASK_NEW  ";
        else if(token  == 4096)
        a =   "TASK_STATE_MAX" ;
	
  return a;
 }
 
 
 static ssize_t my_read(struct file *fp , char* out, size_t size, loff_t * off)
{
 int ret;
        if(out)
        {
        }
        else
      { 
       printk(KERN_ERR"\n Null Pointer ");
       return 0;
      }   
	
    ret = copy_to_user(out, bu, strlen(bu));
    if (ret == 0)
    printk(KERN_INFO"\n Success:All the data has been copied into user space\n"); 
    else
    printk(KERN_ERR"\n Error in copy_to_user() : Bytes left to be copied : %i\n", ret);

    return ret;   	
}
 
 
static int my_close(struct inode *i, struct file *f)
{  
      buffer = bu ;                     
      kfree(buffer);  //DeAllocating the memory allocated by kmalloc()
      printk(KERN_INFO "\n Closing the  /dev/process_list  device\n");
    return 0;
}
 

static struct file_operations my_fops = {
.owner = THIS_MODULE,
.open = my_open,
.read = my_read,
.release = my_close,

};

static struct miscdevice process_list = {
.minor = MISC_DYNAMIC_MINOR,
.name = "process_list",
.fops = &my_fops,
.mode= 0666,
};

static int __init miscdev_init(void)
{ 
        int f;
	printk(KERN_INFO " Init start\n");
	f = misc_register(&process_list);
	if(f<0)
	printk(KERN_ERR "Registeration of  miscdev failed\n");
	else 
        printk(KERN_INFO " Registeration Success\n");	
	return f;	
}

static void  __exit miscdev_exit(void)
{
        misc_deregister(&process_list); 
	printk(KERN_INFO " RMMOD /dev/process_list success\n");
       
		
}

module_init(miscdev_init);
module_exit(miscdev_exit);
