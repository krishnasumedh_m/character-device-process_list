Kernel Modules and Character Device to list all processes
---------------------------------------------------------

EXECUTION PROCESS:

-> Enter administrator mode by typing  'sudo su ' .

-> In my program - 'userspace.c' refers to the program executing in userspace whereas
   'process_list.c' represents the program executing in kernel space.  

-> Then type ' make ' command to compile the userspace program and the kernel space program.

-> 'insmod process_list.ko '   to insert the module into the kernel.

-> ' ./userspace '  to set the program in execution.

-> ' rmmod process_list.ko '   to remove the loaded module from the kernel. 