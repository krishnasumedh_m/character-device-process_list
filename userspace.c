#include <unistd.h>     
#include <fcntl.h>     
#include <stdio.h>
#include <stdlib.h>     
#include <string.h>
#include <sys/types.h>
#define BUFFER_LENGTH  10000

int main(int args, char *argv[])
{
int ret, fd,r;
char *buffer;

buffer = malloc(BUFFER_LENGTH);
   fd = open("/dev/process_list", O_RDWR);             // Open the device with read/write access
   if (fd < 0)
   {
      printf("\n Failed to open the device");
      return 0;  
   }
ret=1;
while(ret>0)       
{
	ret = read(fd, buffer, BUFFER_LENGTH);  // 0 indicates EOF

     //  printf("\n Number of bytes read :%d \n",strlen(buffer));
          
   if (ret < 0)
   {
      printf("\n Failed to read the message from the device.\n");
      
   }

  else
  { 
      r = write(1,buffer,strlen(buffer));   // Display to the console
     // printf("\n Number of bytes writen by write sys call :%d \n", r);
      
     if (r < 0)
      printf("\n Failed to write() \n");    

  }
       
}//end of while() 
   
 close(fd);
 free(buffer);
 return 0;
}// end of main()
